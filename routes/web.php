<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
  //============================================================================
  Route::get('/blog', 'BlogController@index')->name('blog');
  Route::get('/blog/create','BlogController@createBlog')->name('create_blog');
  Route::post('/save','BlogController@saveBlog');
  Route::get('/blog/index','BlogController@showAllBlogs')->name('show_blog');
  Route::get('/hapus/{id_post}','BlogController@hapus')->name('delete_blog');
  Route::get('/edit/{id_post}','BlogController@edit')->name('edit_blog');
  Route::post('/update','BlogController@update');
  Route::get('/view/{id_post}','BlogController@view')->name('view_blog');
  // ===========================================================================
  Route::resource('chatpage', 'MessageController', ['except' => ['show']]);
	Route::get('/chatpage', 'MessageController@index')->name('chatpage');
	Route::get('/message/{id}', 'MessageController@getMessage')->name('message');
	Route::post('message', 'MessageController@sendMessage');
  // ===========================================================================
	Route::get('/home', 'HomeController@index')->name('home');
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
	Route::get('{page}', ['as' => 'page.index', 'uses' => 'PageController@index']);

});
