@extends('layouts.app2',[
'activePage' => 'blogpage',
'namePage' => 'Blogging',
'class' => 'login-page sidebar-mini ',
])
@section('content')
<link rel="stylesheet" href="/css/bootstrap.css">
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/bootstrap-grid.css">
<link rel="stylesheet" href="/css/bootstrap-reboot.css">
<link rel="stylesheet" href="/css/bootstrap-reboot.min.css">
<div class="container">
@if ($errors->any())
 <div class="alert alert-danger">
 <ul>
 @foreach ($errors->all() as $error)
 <li>{{ $error }}</li>
 @endforeach
 </ul>
 </div><br />
@endif
 <div class="card-body mt-5">

   <form method="post" action="/save" enctype="multipart/form-data">
   <div class="form-group">
     <a class="btn btn-secondary" href="/blog" onclick="return confirm('Do you want to go back? All unsaved post will be lost')">Back</a> <br/>
     <input type="hidden" value="{{csrf_token()}}" name="_token" />
     <div class="form-group">

       <p>File Gambar</p><br/>
       <input type="file" name="file" class="form-control-file">
       <!-- <button type="submit" class="btn btn-primary btn-lg btn-block">Upload Gambar</button> -->
     </div>
     <label for="title">Title:</label>
     <input type="text" class="form-control" name="title" required="required"/>

     <div class="form-group">
       <label for=" category" >Intro:Intro: max 300 karakter</label>
       <textarea cols="120" rows="10" class="form-control"  name="intro" required="required"></textarea>
     </div>

     <label for="description">Description:</label>
     <textarea cols="100" rows="20" class="form-control" name="description" required="required"></textarea>

    <button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
   </form>
 </div>

</div>
@endsection
