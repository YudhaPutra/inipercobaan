@extends('layouts.app', [
    'namePage' => 'Welcome',
    'class' => 'login-page sidebar-mini ',
    'activePage' => 'welcome',
    'backgroundImage' => asset('assets') . "/img/bg14.jpg",
])
<link href="css/landing-page.min.css" rel="stylesheet">
@section('content')
  <div class="content">

      <header class="masthead text-white text-center">
        <div class="container">
          <div class="row">
            <div class="col-xl-9 mx-auto">
              <h1 class="mb-5">Find places to explore, people to meet, and memory to make with Travelink</h1>
            </div>
            <!-- <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
              <form>
                <div class="form-row">
                  <div class="col-12 col-md-9 mb-2 mb-md-0">
                    <input type="email" class="form-control form-control-lg" placeholder="Enter your email...">
                  </div>
                  <div class="col-12 col-md-3">
                    <button type="submit" class="btn btn-block btn-lg btn-primary">Sign up!</button>
                  </div>
                </div>
              </form>
            </div> -->
          </div>
        </div>
      </header>

      <!-- Icons Grid -->
      <section class="features-icons bg-light text-center">
        <div class="container">
          <h1 align="center">Features</h1>
          <div class="row">
            <div class="col-lg-4">
              <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                <div class="features-icons-icon d-flex">
                  <img src="{{ url('assets/img/blogging.svg') }}">
                </div>
                <h3>Blogs</h3>
                <p class="lead mb-0">Share your adventure through tons of blog</p>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                <div class="features-icons-icon d-flex">
                  <img src="{{ url('assets/img/message.svg') }}">
                </div>
                <h3>Instant Messaging</h3>
                <p class="lead mb-0">Start a conversation with new people or people you already met</p>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="features-icons-item mx-auto mb-0 mb-lg-3">
                <div class="features-icons-icon d-flex">
                  <img src="{{ url('assets/img/people.svg') }}">
                </div>
                <h3>Create, Share and Edit</h3>
                <p class="lead mb-0">You can create and edit story together with our community</p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- Image Showcases -->
      <section class="features-icons bg-light text-center">
        <div class="p-5">
          <div class="container-fluid p-0">
            <div class="row no-gutters">

              <div class="col-lg-6 order-lg-2 text-white showcase-img"><img src="{{ url('assets/img/Blogging-Tools.jpg') }}"></div>
              <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                <h2 align="left">Tell Your Story through Words</h2>
                <p class="lead mb-0" align="left"> We give you place to write down your journey in one place where everyone see and feel what you felt.</p>
              </div>
            </div>
            <div class="row no-gutters">
              <div class="col-lg-6 text-white showcase-img"><img src="{{ url('assets/img/meeting-new-people.jpg') }}"></div>
              <div class="col-lg-6 my-auto showcase-text">
                <h2 align="right">Meet New People Around You</h2>
                <p class="lead mb-0" align="right">Find new people and start to talk and share what in your mind and explore the world together!</p>
              </div>
            </div>
            <div class="row no-gutters">
              <div class="col-lg-6 order-lg-2 text-white showcase-img"><img src="{{ url('assets/img/travelers.jpg') }}"></div>
              <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                <h2 align="left">From Community to Community</h2>
                <p class="lead mb-0" align="left">Our website is designed for community and to community itself. Feel free to create, share, and change your story together.</p>
              </div>
            </div>
          </div>
        </div>

      </section>

      <!-- Testimonials -->
      <section class="testimonials text-center bg-light">
        <div class="container">
          <h2 class="mb-5">From Us, CodeGenius Team</h2>
          <div class="row">
            <div class="col-lg-4">
              <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                <img class="img-fluid rounded-circle mb-3" src="{{ url('assets/img/programmer.svg') }}" alt="">
                <h5>Anwaruddin Ridho N</h5>
                <p class="font-weight-light mb-0"></p>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                <img class="img-fluid rounded-circle mb-3" src="{{ url('assets/img/programmer.svg') }}" alt="">
                <h5>Nugroho Fadillah Yudha Putra</h5>
                <p class="font-weight-light mb-0"></p>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="testimonial-item mx-auto mb-5 mb-lg-0">
                <img class="img-fluid rounded-circle mb-3" src="{{ url('assets/img/programmer.svg') }}" alt="">
                <h5>M. Yusril Nugraha Putra</h5>
                <p class="font-weight-light mb-0"></p>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- Call to Action -->
      <section class="call-to-action text-white text-center">
        <div class="overlay"></div>
        <div class="container">
          <div class="row">
            <div class="col-xl-9 mx-auto">
              <h2 class="mb-4">Ready to get started? Sign up now!</h2>
            </div>
            <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
              <a class="btn btn-block btn-lg btn-primary" href="/register">Sign In</a>
              <!-- <form>
                <div class="form-row">
                  <div class="col-12 col-md-9 mb-2 mb-md-0">
                    <input type="email" class="form-control form-control-lg" placeholder="Enter your email...">
                  </div>
                  <div class="col-12 col-md-3">
                    <button type="submit" class="btn btn-block btn-lg btn-primary">Sign up!</button>
                  </div>
                </div>
              </form> -->
            </div>
          </div>
        </div>
      </section>

  </div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
      demo.checkFullPageBackgroundImage();
    });
  </script>
@endpush
