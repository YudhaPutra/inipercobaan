@extends('layouts.app2',[
'activePage' => 'blogpage',
'namePage' => 'Blogging',
'class' => 'login-page sidebar-mini ',
])

@section('content')
<link rel="stylesheet" href="/css/bootstrap.css">
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/bootstrap-grid.css">
<link rel="stylesheet" href="/css/bootstrap-reboot.css">
<link rel="stylesheet" href="/css/bootstrap-reboot.min.css">
<div class="container">
@if ($errors->any())
 <div class="alert alert-danger">
 <ul>
 @foreach ($errors->all() as $error)
 <li>{{ $error }}</li>
 @endforeach
 </ul>
 </div><br />
@endif
<!-- enctype="multipart/form-data" -->
>
@foreach($blogs as $blog)
 <div class="card-body mt-5">
   <a class="btn btn-secondary" href="/blog" >Back</a> <br/>
    <img src="{{ url('/data_file/'.$blog->file) }}"  class="card-img-top" alt="...">
    <div class="card-body">
      <h3 class="card-title">{{$blog->title}}</h3>
      <hr/>
      <p class="card-text">{{$blog->intro}}</p>
      <hr/>
      <p class="card-text" style="white-space: pre-line">{{$blog->description}}</p>
      <p class="card-text"><small class="text-muted">Dibuat pada: {{$blog->updated_at}}</small></p>
    </div>
  </div>

    <a  class="btn btn-info" href="/edit/{{$blog->id}}">Edit</a>
    <a class="btn btn-danger"  href="/hapus/{{$blog->id}}" onclick="return confirm('Do you want to delete this post?')">Delete {{$blog->id}}</a>

</div>
@endforeach
@endsection
