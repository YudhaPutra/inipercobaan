@extends('layouts.app2',[
'activePage' => 'chatpage',
'namePage' => 'ChatPage',
'class' => 'login-page sidebar-mini ',

])


@section('content')

<div class="card-body mt-5">
    <div class="row">
        <div class="col-md-4">
            <div class="user-wrapper">
                <ul class="users">
                    @foreach($users as $user)
                        <li class="user" id="{{ $user->id }}">
                            {{--menunjukkan berapa pesan yang belum terbaca--}}
                            @if($user->unread)
                                <span class="pending">{{ $user->unread }}</span>
                            @endif

                            <div class="media">
                                <div class="media-left">
                                    <img src="{{ $user->avatar }}" alt="" class="media-object">
                                </div>

                                <div class="media-body">
                                    <p class="name">{{ $user->name }}</p>
                                    <p class="email">{{ $user->email }}</p>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="col-md-8" id="messages">


        </div>

  </div>
</div>
@endsection
