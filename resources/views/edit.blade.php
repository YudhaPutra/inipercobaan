@extends('layouts.app2',[
'activePage' => 'blogpage',
'namePage' => 'Blogging',
'class' => 'login-page sidebar-mini ',
])

@section('content')
<div class="container">

@if ($errors->any())
 <div class="alert alert-danger">
 <ul>
 @foreach ($errors->all() as $error)
 <li>{{ $error }}</li>
 @endforeach
 </ul>
 </div><br />
@endif
<!-- enctype="multipart/form-data" -->
 @foreach($blogs as $blog)
 <div class="card-body mt-5">

   <form method="post" action="/update" >
     <div class="form-group">
       <a class="btn btn-secondary" href="/view/{{$blog->id}}" onclick="return confirm('Do you want to go back?'All unsaved post will be lost)">Back</a> <br/>
       <input type="hidden" value="{{csrf_token()}}" name="_token"/>
       <input type="hidden" name="id" value="{{$blog->id}}" required="required">
      <!-- <div class="form-group"> -->
       <!-- <p>File Gambar</p><br/> -->
       <!-- <input type="file" name="file"> -->
    <!-- </div> -->
       <label for="title">Title:</label>
       <input type="text" class="form-control" name="title" value="{{$blog->title}}" required="required"/>
     </div>
     <div class="form-group">
       <label for="title">Intro: max 300 karakter</label>
       <textarea cols="5" rows="5" class="form-control" name="intro" required="required" maxlength="300">{{$blog->intro}}</textarea>
     </div>
     <div class="form-group">
       <label for="description">Description:</label>
       <textarea cols="100" rows="20" class="form-control" name="description" required="required">{{$blog->description}}</textarea>
     </div>
       <button type="submit" class="btn btn-primary btn-lg btn-block" onclick="return confirm('Do you want to proceed? This post will be saved and the old save will be overwritten')">Submit</button>
   </form>

</div>
</div>
@endforeach
@endsection
