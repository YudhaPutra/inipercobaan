@extends('layouts.app2',[
'activePage' => 'blogpage',
'namePage' => 'Blogging',
'class' => 'login-page sidebar-mini ',
])

@section('content')
<!-- style="background-color: #eee;" -->
<div class="w-auto p-5">
  <!-- <div class="mx-auto" style="width: 600px;">
    <h1>Welcome to The Blog Section</h1>
  </div> -->
  <!-- <img src="{{ url('/data_file/images.jpg') }}" class="mw-100" style="object-fit: cover;"> -->
</div>
<link rel="stylesheet" href="/css/bootstrap.css">
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/bootstrap-grid.css">
<link rel="stylesheet" href="/css/bootstrap-reboot.css">
<link rel="stylesheet" href="/css/bootstrap-reboot.min.css">
<div class="container">
  <a href="/blog/create" class="btn btn-primary btn-lg btn-block">Add New Blog</a>
 @if(\Session::has('success'))
 <div class="alert alert-success">
 {{\Session::get('success')}}
 </div>
 @endif
 <br/>
  @foreach($blogs->reverse() as $blog)
 <div class="card mb-3">
   <div class="card-body">
   <img src="{{ url('/data_file/'.$blog->file) }}" class="card-img" >
   <hr/>
    <a href="/view/{{$blog->id}}">
      <h2 class="card-title">{{$blog->title}}</h2>
    </a>
     <hr/>
     <p>{{$blog->intro}}</p>
     <p class="card-text"><small class="text-muted">Dibuat pada: {{$blog->updated_at}}</small></p>
     <!-- <a  class="btn btn-info" href="/view/{{$blog->id}}">View</a> -->

   </div>
 </div>
 @endforeach
@endsection
