<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gambar extends Model
{
  protected $table = "blogs";

  protected $fillable = ['file'];
}
