<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
  protected $table = "blogs";
  protected $fillable = ['file'];

  public function createBlog($data)
  {

    $this->user_id = auth()->user()->id;
    $this->title = $data['title'];
    $this->description = $data['description'];
    $this->intro = $data['intro'];
    // $this->file = $data['file'];
    $this->save();
    // protected $table = "blog";
    // protected $fillable = ['file'];
    return 1;
  }

}
