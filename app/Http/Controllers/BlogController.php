<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\Gambar;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    public function __construct()
  {
    $this->middleware('auth');
  }
  public function createBlog()
  {
    return view('create');
  }

  public function index()
    {

    	$blog = DB::table('blogs')->get();

    	// mengirim data pegawai ke view index

    	return view('index',['blogs' => $blog]);

    }

  public function saveBlog(Request $request)
   {

     $blog = new Blog();

     $this->validate($request, [
     'title'=>'required',
     'intro'=>'required',
     'description'=>'required',
     'file' => 'required|file|image|mimes:jpeg,png,jpg|max:2048'
     ]);

     if ($request->hasFile('file')) {
         $file = $request->file('file');
         $nama_file = $file->getClientOriginalName();
         $tujuan_upload = 'data_file';
         $file->move($tujuan_upload,$nama_file);
         $blog->file = $nama_file;
       }

    //  $file = $request->file('file');
    //
    //  $nama_file = time()."_".$file->getClientOriginalName();
    //   // isi dengan nama folder tempat kemana file diupload
 		//  $tujuan_upload = 'data_file';
    //  $file->move($tujuan_upload,$nama_file);
    //  // 'file' = $nama_file;
    //
    //  Gambar::create([
 		// 	'file' => $nama_file
 		// ]);

    $blog->createBlog($request->all());
    return redirect('/blog')->with('Sukses', 'Blog baru sudah dibuat :D');// the index method is the list blogs method that we will create in further lines


  }

   public function showAllBlogs()
   {
     $blogs = Blog::where('user_id', auth()->user()->id)->get();

     return view('index',compact('blogs'));
   }

   public function edit($id)
    {
      $blog = DB::table('blogs')->where('id',$id)->get();
      //passing data ke view edit.blade.php
      return view('edit',['blogs'=>$blog]);
    }

    public function update(Request $request)
    {


        DB::table('blogs')->where('id', $request->id)->update([
          'title'=>$request->title,
          'intro'=>$request->intro,
          'description'=>$request->description,
          // 'file' => $request->file
        ]);

        // $request->hasFile('file');
        // $file = $request->file('file');
        // $nama_file = $file->getClientOriginalName();
        // $tujuan_upload = 'data_file';
        // $file->move($tujuan_upload,$nama_file);

      //kembali ke halaman /barang untuk menampilkan data hasil update
      return redirect('/blog');
    }

   public function hapus($id)
    {
      DB::table('blogs')->where('id',$id)->delete();
      return redirect('/blog');
    }

    public function view($id)
    {
      $blog = DB::table('blogs')->where('id',$id)->get();
      //passing data ke view edit.blade.php
      return view('view',['blogs'=>$blog]);
    }
}
